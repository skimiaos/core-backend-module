<?php

namespace Skimia\Backend\Facades;

use \Illuminate\Support\Facades\Facade;

class Tiles extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Backend\Managers\Tiles';
    }
}