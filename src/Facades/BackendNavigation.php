<?php

namespace Skimia\Backend\Facades;

use \Illuminate\Support\Facades\Facade;

class BackendNavigation extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Backend\Managers\BackendNavigation';
    }
}