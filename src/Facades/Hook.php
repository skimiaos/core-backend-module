<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 31/05/2015
 * Time: 20:31
 */

namespace Skimia\Backend\Facades;
use \Illuminate\Support\Facades\Facade;


class Hook extends Facade {

    protected static function getFacadeAccessor(){
        return 'Skimia\Backend\Managers\Hook';
    }
}