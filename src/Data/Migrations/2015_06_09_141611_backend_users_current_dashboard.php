<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Skimia\Backend\Data\Models\Dashboard;
class BackendUsersCurrentDashboard extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function(Blueprint $table)
        {
            $dashboard_id = Dashboard::first(['id'])->id;
            $table->unsignedInteger('current_dashboard_id')->default($dashboard_id);
            $table->boolean('user_visible')->default(true);

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function($table)
        {
            $table->dropColumn('current_dashboard_id');
            $table->dropColumn('user_visible');
        });
	}

}
