<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Skimia\Backend\Data\Models\Dashboard;

class BackendDashboards extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('backend_dashboards')){
            Schema::create('backend_dashboards', function($table){
                $table->increments('id');
                $table->string('name')->unique();
                $table->text('description')->nullable();
                $table->string('bg')->nullable();

                $table->text('show_rules')->nullable();
                $table->text('edit_rules')->nullable();

                $table->text('sections');
                $table->text('tiles');

                $table->timestamps();

            });
        }
        $dashboard = new Dashboard();
        $dashboard->name = 'WebDesigner';
        $dashboard->description = 'un menu optimisé pour les designers web';
        $dashboard->bg = '#FFF';
        $dashboard->show_rules = 'backend dashboard_designer';
        $dashboard->edit_rules = 'read_only';

        $dashboard->setSections($this->getSections(true));

        $dashboard->setTiles($this->getTiles(true));


        $dashboard->save();

        $dashboard = new Dashboard();
        $dashboard->name = 'Dashboard';
        $dashboard->description = 'un menu optimisé pour les designers web';
        $dashboard->bg = '#FFF';
        $dashboard->show_rules = 'backend dashboard_user';
        $dashboard->edit_rules = 'read_only';

        $dashboard->setSections($this->getSections(false));

        $dashboard->setTiles($this->getTiles(false));


        $dashboard->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('backend_dashboards');
    }



    public function getSections($admin = false){
        $event_return = Event::fire('skimia.backend::seed.dashboard.default.sections', [$admin]);

        $temp_sections = [];
        $sections = [];
        foreach($event_return as $return){
            $temp_sections = array_merge($temp_sections,$return);
        }
        $i = 1;
        foreach($temp_sections as $key=>$value){
            $sections[$key] = [
                'name'=>$value,
                'position'=>$i
            ];

            $i++;
        }

        return $sections;
    }

    public function getTiles($admin = false){
        $event_return = Event::fire('skimia.backend::seed.dashboard.default.tiles', [$admin]);


        $tilesgrid = [];

        foreach($event_return as $event_r) {
            foreach ($event_r as $section => $list) {

                if (!isset($tilesgrid[$section]))
                    $tilesgrid[$section] = [];

                foreach ($list as $key => $tile) {
                    $tilesgrid[$section][$key] = $tile;
                }
            }
        }

        foreach($tilesgrid as $section=>$list){
            $i = 1;

            foreach($list as $key=>$tile){
                $tilesgrid[$section][$key]['position'] = $i;

                $i++;
            }


        }

        return $tilesgrid;
    }
}