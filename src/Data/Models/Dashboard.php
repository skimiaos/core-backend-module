<?php

namespace Skimia\Backend\Data\Models;

use Eloquent;

class Dashboard extends Eloquent {

    protected $table = 'backend_dashboards';


    public function toArray(){

        return array_merge(parent::toArray(),[
            'sections'=>unserialize($this->sections),
            'tiles'=>unserialize($this->tiles),

        ]);
    }

    public function setSections($sections){
        $this->sections = serialize($sections);
        return $this;
    }

    public function setTiles($tiles){
        $this->tiles = serialize($tiles);
        return $this;
    }

}