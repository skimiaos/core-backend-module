<?php

namespace Skimia\Backend\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;

use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;

class RolesActionsCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Role();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.backend::form.roles_actions');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $options->Fields()->makeTextField('name')
            ->setTranslatedLabel()
            ->setTranslatedInfo()
        ->setDisplayOrder(1000);
    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-key-4')->setTitle('Gestion des autorisations par Groupes');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-user-4')->setTitle('Créer un nouveau Groupe');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-user-4')->setTitle('Editer les autorisations du Groupe');//TRANSFO RELATIONNELLES

        $this->listConfiguration->addIdColumn(false);
        $nameCol = $this->listConfiguration->getNewColumnDefinition('name')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();

        $nameCol->getNewFilterDefinition()->setInputFilter('Contient')->setConditionFilter(ListCrudFilterConfiguration::CONTAINS);

        $options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave','Actions et droits modifiées pour le groupe "%name%"');
        $options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave','Nouveau groupe "%name%" crée');

        $this->makeActionField($options);

    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'roles_actions';
    }


    protected function makeActionField($options){
        $actionsField = $options->ActionFields(self::$EDIT_REST_ACTION)->makeAssociativeField('actions');

        $actionsField->setGenerating(function(Options\Fields\AssociativeField $field){
            $actions = $this->getAcl()->actions()->get();
            foreach ($actions as $action) {
                $field->Fields()->makeCheckboxField($action)
                    ->setLabel($action);

            }
        });
        $actionsField->setBladeTemplate('skimia.backend::crud.fields.roles-actions');
        $actionsField->setTwoWayField(
            function(&$entity,&$data,$key,$field){

                $acl = \App::make('acl');
                $actions = [];
                foreach($field['fields'] as $key=>$value){

                    $actions[$key] = $acl->check($entity['name'],$key);

                }
                $data = $actions;

            },
            function(&$entity,&$data,$key,$field){

                $name = $entity->getOriginal()['name'];
                foreach($data as $action=>$can){
                    $this->getAcl()->allow($name,[$action],$can);
                }


            });
    }
}