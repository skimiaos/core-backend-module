<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 01/01/2015
 * Time: 23:14
 */

namespace Skimia\Backend\Data\Forms;

use Skimia\Angular\Form\AngularTrait;
use Skimia\Form\Base\Form;

class Users extends Form{

    use AngularTrait;

    protected $template='skimia.backend::auth.login_form-theme';

    protected $fields = [
        'email'=>[
            'type'=>'text',
            'label'=> 'Email',
            'required'
        ],
        'password'=>[
            'type'=>'password',
            'label'=>'Mot de Passe',
            'required',//un des deux avec celui-ci dessous est interpreté(en premier 'required'=>true puis 'required' enfin 'rules'=>['required'] )
            'rules'=>[
                'required',
                'min:5',
                'max:32'
            ]
        ],
        'remember'=>[
            'type'=>'checkbox',
            'label'=>'Se souvenir de moi',
        ]
    ];
}