<?php

namespace Skimia\Backend\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Backend\Data\Models\Dashboard;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;

class AccountsCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new \User();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.backend::form.accounts');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $options->Fields()->makeTextField('email')
            ->transAll()
        ->setDisplayOrder(1000);





        $options->Fields()->makeTextField('fullname')
            ->transAll()
            ->setDisplayOrder(2000);




        $options->Fields()->makeTextField('fullname')
            ->transAll()
            ->setDisplayOrder(2000);

        $options->Fields()->makeSelectField('status')
            ->transAll()
            ->setChoices([
                \User::UNVERIFIED => 'Non Vérifié',
                \User::SUSPENDED => 'Suspendu',
                \User::VERIFIED => 'Vérifié'

            ])->setDisplayOrder(3000);
        $options->Fields()->makeRelationField('roles')->ManyToManyRelation()->displayColumns('name')->transAll()->setDisplayOrder(4000);


        $options->Fields()->makePasswordField('change_password')
            ->transAll()
            ->setDisplayOrder(5000);

        $options->Fields()->makeSelectField('current_dashboard_id')
            ->transAll()
            ->setProgrammingChoices(function(){
                return Dashboard::lists('name','id');
            })->setDisplayOrder(6000);

        $options->Fields()->makeCheckboxField('user_visible')
            ->transAll()
            ->setDisplayOrder(7000);

        $options->Fields()->setDisplayRoutine(function($k,&$v){

            if($k == 'status' || $k == 'roles'|| $k == 'current_dashboard_id')
                return $this->getAcl()->can('full_users_gestion');

            if($k == 'user_visible')
                return $this->getAcl()->can('view_hidden_users');

            return true;
        });


        $this->registerCRUDFormEvent('beforeSave',function($entity){
            if(isset($entity->change_password)){
                $entity->password= $entity->change_password;
                unset($entity->change_password);
            }
        });

        $this->registerCRUDFormEvent('beforeCreateSave',function($entity){
            if(!isset($entity->current_dashboard_id)){
                $entity->current_dashboard_id = \Auth::user()->current_dashboard_id;
            }
            if(!isset($entity->status)){
                $entity->status = 1;
            }
        });

        $this->registerCRUDFormEvent('afterCreateSave',function($entity){
            if(count($entity->roles)== 0){
                foreach(\Auth::user()->roles as $role){
                    $entity->roles()->attach($role->id);
                }
            }
            $entity->save();
        });
    }

    protected function configureActions(ActionOptionsInterface $options)
    {

        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-users-1')->setTitle('Liste des Comptes utilisateurs');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-user-4')->setTitle('Créer un nouvel utilisateur');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-user-4')->setTitle('Edition d\'un utilisateur');//TRANSFO RELATIONNELLES
        $options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);

        $this->listConfiguration->addIdColumn();
        $email = $this->listConfiguration->getNewColumnDefinition('email')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();
        $fullname = $this->listConfiguration->getNewColumnDefinition('fullname')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();
        $status = $this->listConfiguration->getNewColumnDefinition('status')->type(ListCrudColumnConfiguration::_TYPE_SELECT)->automaticTranslatedDisplayName();
        $roles = $this->listConfiguration->getNewColumnDefinition('roles')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();



        $options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave','Utilisateur "%fullname%" edité');
        $options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave','Nouvel utilisateur "%fullname%" crée');


    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'accounts';
    }

}