<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 17/03/2015
 * Time: 18:56
 */

namespace Skimia\Backend\Data\Forms;

use Orchestra\Model\Role;
use Skimia\Angular\Form\CrudForm;
use Skimia\Auth\Traits\Acl;
use Skimia\Backend\Data\Models\Dashboard;

class UsersCrudForm extends CrudForm{

    use Acl;
    protected $name = 'users';

    protected $list = true;
    protected $get = false;

    protected function getQuery($params = [])
    {
        if($this->getAcl()->can('view_hidden_users'))
            return parent::getQuery($params);
        else
            return parent::getQuery($params)->where('user_visible',true);
    }

    protected $edit = true;
    protected $create = true;
    protected $delete = true;

    protected $hiddenFields =  ['created_at','deleted_at','updated_at','id','password','remember_token','user_visible','current_dashboard_id','avatar'];

    protected $langKey = 'skimia.backend::form.users';

    protected $dateFields = false;

    protected $allowBatch = true;


    /*protected $getFields = [
        'id'=>['type'=>'text','label'=>'Id'],
        'email'=>['type'=>'text','label'=>'Email'],
        'fullname'=>['type'=>'text','label'=>'Nom Complet']
    ];*/

    protected $fields = [
        'status'=> ['type'=>'select',
            'default'=> \User::VERIFIED,
            'choices'=>[
            \User::UNVERIFIED => 'Non Vérifié',
            \User::SUSPENDED => 'Suspendu',
            \User::VERIFIED => 'Vérifié'
        ]],
        'roles'=>[
            'type'=>'relation',
            'relation'=>'many_to_many',
            'show_collumn'=>'name'
        ],
        'email'=>[
            'type'=>'text',
            'label'=> 'Email',
        ],
        'fullname'=>[
            'type'=>'text',
            'label'=> 'Nom Complet'
        ],
        'change_password'=>[
            'type'=>'password',
            'label'=>'Changer le mot de passe'
        ]
    ];
    protected $listFields = [
        'id'=>['type'=>'text','label'=>'ID'],
        'email'=>['type'=>'text','label'=>'Email'],
        'fullname'=>['type'=>'text','label'=>'Nom Complet'],
        'status'=> ['type'=>'select',
            'label'=>'Statut',
            'choices'=>[
                \User::UNVERIFIED => 'Non Vérifié',
                \User::SUSPENDED => 'Suspendu',
                \User::VERIFIED => 'Vérifié'
            ]],
        'roles'=>[
            'label'=>'Roles',
            'type'=>'relation',
            'relation'=>'many_to_many',
            'show_collumn'=>'name'
        ],
    ];

    protected $listItemActions = [

    ];


    protected function getNewEntity()
    {
        return new \User();
    }

    protected function makeFields()
    {
        if(!$this->getAcl()->can('full_users_gestion')){

            unset($this->listFields['id']);
            unset($this->listFields['status']);
            unset($this->listFields['roles']);

            $this->hiddenFields[] = 'id';
            $this->hiddenFields[] = 'status';
            $this->hiddenFields[] = 'roles';

            unset($this->fields['id']);
            unset($this->fields['status']);
            unset($this->fields['roles']);


        }else{

            $this->fields['current_dashboard_id'] =[
                'label'=>'Dashboard Utilisateur',
                'type'=>'select',
                'choicesFct'=>function(){
                    return Dashboard::lists('name','id');
                }
            ];
            if($this->getAcl()->can('view_hidden_users'))
                $this->fields['user_visible'] =[
                    'label'=>'Utilisateur Visible',
                    'type'=>'checkbox'
                ];
        }

        parent::makeFields();
    }

    protected function beforeSave($entity){
        if(isset($entity->change_password)){
            $entity->password= $entity->change_password;
            unset($entity->change_password);
        }
        return parent::beforeSave($entity);
    }

    protected function beforeCreateSave($entity){
        if(!isset($entity->current_dashboard_id)){
            $entity->current_dashboard_id = \Auth::user()->current_dashboard_id;
        }
        if(!isset($entity->status)){
            $entity->status = 1;
        }
        return $this->beforeSave($entity);
    }

    protected function afterCreateSave( $entity){
        if(count($entity->roles)== 0){
            foreach(\Auth::user()->roles as $role){
                $entity->roles()->attach($role->id);
            }
        }
        $entity->save();
        return parent::afterCreateSave($entity);
    }

}