<?php

namespace Skimia\Backend;

use Skimia\Auth\Facades\AclActions;
use Skimia\Modules\ModuleBase;
use Theme;
use Event;
class Module extends ModuleBase{

    public function preBoot(){

    }

    public function register(){
        parent::register();
    }


    public function getAliases(){
        return [
            'Tiles' => 'Skimia\Backend\Facades\Tiles',
            'Hook' => 'Skimia\Backend\Facades\Hook',
        ];
    }


    public function beforeRegisterModules(){

        Event::listen('artisan.os.install.acl',function($manager){
            $manager->attachActions([
                'backend',
                'dashboard view',
                'dashboard create',
                'dashboard view all',
            ]);
            $acl = $manager->getAcl();
            $acl->allow('Member', ['backend']);
        });
    }
} 