<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 13/01/2015
 * Time: 20:35
 */

namespace Skimia\Backend\Managers;


use Illuminate\Support\Collection;

class Tiles {

    protected $tiles = [];

    public function __construct(){
        $this->tiles = new Collection($this->tiles);

        \Event::listen('skimia.angular::states.render_state',function($application,$states){
            return $this->updateStates($application,$states);
        });
    }

    public function makeFromState($id, $state,$stateParams, $name, $icon = false, $bg = false){

        $this->tiles->put($id,[
            'id'=>$id,
            'name'=>$name,
            'state'=>$state,
            'stateParams'=>$stateParams,
            'icon'=>$icon,
            'bg'  =>$bg
        ]);
        return $this;
    }

    public function findByState($state){
        foreach($this->tiles->toArray() as $id=>$tile){
            if($tile['state'] == $state){
                return $tile;
            }
        }
        return false;
    }

    public function updateStates($application,$states){

        if($application == OS_APPLICATION_NAME){
            $states_tiles = [];

            $created_activities = [];
            foreach($this->tiles as $tile){
                if(isset($tile['state'])){
                    $states_tiles[$tile['state']] = $tile;
                }
            }
            ksort($states_tiles);
            $states_tiles['tilescreen'] = [];
            foreach($states_tiles as $state=>$tile){
                if($this->getParentState($created_activities,$state) === false){
                    $created_activities[$state] = $tile;


                    if(!isset($states[$state])){
                        throw new \Exception('[TilesManager::updateStates] le state "'.$state.'" est introuvable');
                    }

                    $modified_state = $states[$state];

                    $modified_state['views'] = [
                        $state.'@' => [
                            'controller' => $modified_state['controller'],
                            'templateUrl' => $modified_state['templateUrl']
                        ]
                    ];
                    unset($modified_state['controller']);
                    unset($modified_state['templateUrl']);

                    if(!isset($modified_state['deepStateRedirect']))
                        $modified_state['deepStateRedirect'] = true;
                    $modified_state['sticky'] = true;
                    if($state !='tilescreen')
                        $modified_state['activity'] = $state;
                    $states[$state] = $modified_state;
                }

            }
            foreach($states as $name => $state){
                if($this->getParentState($created_activities,$name) !== false && $name != 'tilescreen'){
                    $states[$name]['activity'] = $this->findByState($this->getParentState($created_activities,$name));
                }
            }
            return $states;
        }
    }
    protected function getParentState($states,$state_name){
        $sections = explode('.',$state_name);

        $current_section = $sections[0];
        unset($sections[0]);
        if(array_key_exists($current_section,$states))
            return $current_section;
        foreach($sections as $section){
            $current_section .= '.'.$section;
            if(array_key_exists($current_section,$states))
                return $current_section;
        }
        return false;
    }

    public function all(){
        return $this->tiles->toArray();
    }

}