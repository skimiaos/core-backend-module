<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 22/10/2014
 * Time: 19:06
 */

namespace Skimia\Backend\Managers;

use Event;
use Config;
use Illuminate\Support\Collection;

class Hook {

    protected $hooks;

    public function __construct(){
        $this->hooks = new Collection();
    }
    public function register($hook, $callable, $priority = 0){
        Event::listen('hook.'.$hook, $callable, $priority);
    }

    public function call($hook, $data = array()){

        $bridge = new Bridge($this->getStructure($hook),$data);
        Event::fire('hook.'.$hook,$bridge);
        return $bridge->getResults();
    }

    public function define($hook, $structure){
        $default = Config::get('skimia.backend::hook.structure');

        $this->hooks->put($hook, array_merge($default,$structure));

    }

    /**
     * @param $hook
     * @return mixed
     * @todo voir pour "handle" des hook structures avec des etoiles pas
     */
    protected function getStructure($hook){
        if($this->hooks->has($hook))
            return $this->hooks->get($hook, Config::get('skimia.backend::hook.structure'));
        else{
            $current_hook = $hook;
            $hook_explode = explode('.',$hook);
            for($i = count($hook_explode)-1;$i > 0;$i--){
                $hook_explode[$i] = '*';

                if($this->hooks->has(implode('.',$hook_explode)))
                    return $this->hooks->get(implode('.',$hook_explode), Config::get('skimia.backend::hook.structure'));

            }
        }
        return Config::get('skimia.backend::hook.structure');

    }


} 