<?php

namespace Skimia\Backend\Managers;

class BackendNavigation {

    protected $navigation = [
        'Parametres' => [],
    ];

    public function addMenu($name, $config = []){
        if(!key_exists($name, $this->navigation)){
            $this->navigation[$name] = $config;
        }
        else{
            $this->navigation[$name] = array_merge($this->navigation[$name], $config);
        }
    }

    public function render(){
        return $this->navigation;
    }

} 