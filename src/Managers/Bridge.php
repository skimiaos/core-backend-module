<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/10/2014
 * Time: 06:40
 */

namespace Skimia\Backend\Managers;

use Illuminate\Support\Collection;

class Bridge {

    protected $structure = null;

    protected $results = [];
    protected $event = null;

    public function __construct(array $structure, $eventData){
        $this->initStructure($structure);
        $this->initData($eventData);
    }

    protected function initStructure(array $structure){
        $this->structure = Collection::make($structure);

        $this->results = [];
        foreach($structure as $key=>$value){
            $this->results[$key] = [];
        }
    }

    protected function initData($eventData){
        $this->event = Collection::make($eventData);
    }

    public function __set($varName, $varValue){

        if($this->hasModifiers($varName,['list','merge'])){
            $this->results[$varName] =array_merge($this->results[$varName],$varValue);
        }
        else
            $this->results[$varName][] = $varValue;
    }

    protected function hasModifiers($varName, $modifier = []){
        $varmodifiers = explode('|',$this->structure->get($varName));
        $varmodifiers = array_map(function($n){ return trim($n);},$varmodifiers);

        foreach ($modifier as $mod) {
            if(!in_array($mod,$varmodifiers)){
                return false;
            }
        }
        return true;

    }
    public function __get($varName){
        if($varName == "event")
            return $this->event;
        if($varName == "options"){
            return $this->event->get('hook');
        }
        if(!isset($this->results[$varName])){
            throw new Exception('le membre "(paramètre1)Bridge->'.$varName.'" n\'existe pas');
        }
        return Collection::make($this->results[$varName]);
    }

    public function __call($method, $arguments){
        if(substr($method,0,3) == 'add' ){
            $varName = lcfirst(substr($method,3));
            if(isset($this->structure[$varName]) && (is_array($this->structure[$varName]) || $this->structure[$varName] == 'list' )){

                if(count($arguments) > ($this->structure[$varName] == 'list' ? 1 : count($this->structure[$varName]))){
                    throw new Exception('Impossible');
                }

                if(is_array($this->structure[$varName]))
                {
                    $varValue = array();
                    foreach($arguments as $i => $value){
                        $varValue[$this->structure[$varName][$i]] = $arguments[$i];

                    }
                    $this->results[$varName][] = Collection::make($varValue);

                }
                else
                    $this->results[$varName][] = $arguments[0];

            }
            else
                throw new Exception('Impossible');
        }
        else
            throw new Exception('Impossible');

        return $this;
    }

    public function getResults(){
        return Collection::make($this->results);
    }
} 