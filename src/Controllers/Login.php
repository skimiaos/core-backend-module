<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 07/11/2014
 * Time: 19:39
 */

namespace Skimia\Backend\Controllers;

use Controller;
use Skimia\Angular\Facades\AResponse;
use Skimia\Auth\Processor\Credentials;
use Skimia\Auth\Validation\Auth as AuthValidator;
use View;
use Input;
use Angular;
use Skimia\Auth\Traits\Acl;

class Login extends Controller{

    use Acl;
    protected $processor;

    public function __construct(){
        $this->processor = new Credentials(new AuthValidator());
    }


    //Simple REST
    public function login()
    {
        return $this->processor->login($this, Input::all());
    }

    public function logout()
    {
        return $this->processor->logout($this);
    }

    /**
     * Response when validation on login failed.
     *
     * @param  object  $validation
     * @return Response
     */
    public function loginValidationFailed($validation)
    {
        return AResponse::setErrors($validation->errors())->r();
    }

    /**
     * Response when login failed.
     *
     * @return Response
     */
    public function loginFailed()
    {

        return AResponse::addMessage(trans('skimia.auth::response.credential.invalid-combination'),'danger')->r();

    }

    /**
     * Response when login succeed.
     *
     * @return Response
     */
    public function loginSucceed()
    {
        \Event::fire('skimia.backend::login.succeed',[$this->getAcl()]);

        return AResponse::r();

    }

    /**
     * Response when logout succeed.
     *
     * @return Response
     */
    public function logoutSucceed()
    {

        return AResponse::redirect(route('angular.application.backend.login'))->addMessage(trans('skimia.auth::response.credential.logged-out'),'warning')->r();

    }

}
