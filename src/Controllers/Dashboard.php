<?php

namespace Skimia\Backend\Controllers;

use Angular;
use Controller;
use Skimia\Backend\Data\Forms\Users;
use Skimia\Backend\Data\Models\Dashboard as Dash;

class Dashboard extends Controller{


    public function init(){
        \Angular::get(OS_APPLICATION_NAME)->isSecure();

        //recupération de toutes les tiles statiques
        $static = \Tiles::all();

        //TODO vérifier les droits show_rules
        //récuperation du dashboard par default de la personne
        $dashboard = Dash::find(\Auth::user()->current_dashboard_id)->toArray();

        return \AResponse::r([
            'static_tiles'=>$static,
            'dashboard'=>$dashboard
        ]);
    }


    public function get($id){
        \Angular::get(OS_APPLICATION_NAME)->isSecure();

        $board = Dash::find($id);

        //TODO vérifier les droits show_rules
        return $board;

    }

    public function all(){
        \Angular::get(OS_APPLICATION_NAME)->isSecure();

        $boards = Dash::all(['id','name','description']);

        //TODO retourner uniquement les dashboard visibles(show_rules OK)
        return $boards;
    }

} 