<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 28/02/2015
 * Time: 11:46
 */


return [
    'url'=>'/backend',
    'name'=>'Skimia OS',
    'login'=> [
        'brand'=>'Skimia OS',
        'title'=>'Administration de votre suite logicielle',
        'button'=> 'Connexion',
        'colors'=> [
            'main'=>'#c0392b',
            'second'=>'#e74c3c'
        ]
    ]
];