<?php



Route::group(array('namespace' => 'Skimia\Backend\Controllers'), function()
{

    /**
     * Angular Section
     */
    $app = Angular::get(OS_APPLICATION_NAME);

    $app->addState('tilescreen','/','skimia.backend::dashboard','Skimia\Backend\Controllers\Dashboard@init');

    Route::get('/backend/dashboard/list', array('before'=>'angular.call:dashboard.list', 'uses' => 'Dashboard@all'));
    Route::get('/backend/dashboard/{id}', array('before'=>'angular.call:dashboard.find', 'uses' => 'Dashboard@get'));


    Route::any('/test/form',array('uses'=>'Dashboard@formulaire'));

});

