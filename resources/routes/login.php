<?php

$app = Angular::get(OS_APPLICATION_NAME.'.login');

//$app->addState('home','/','skimia.backend::auth.partials.index','Skimia\Backend\Controllers\Login@index');

Route::group(array('namespace' => 'Skimia\Backend\Controllers'), function()
{



    /**
     * Angular Section
     */

    /*Angular::Application('backend.login','Login@app','/backend/login')
        ->state('home','/home','Skimia\Backend\Controllers\Login@index');
    */
    Route::post('login', array('before'=>'angular.call:backend.login', 'as' => 'backend::login', 'uses' => 'Login@login'));

    Route::any('logout', array('before'=>'angular.call:backend.logout', 'as' => 'backend::logout', 'uses' => 'Login@logout'));
});