<?php

return [
    'users' => [
        'id' => ['label'=>'Id'],
        'email' => ['label'=>'Email'],
        'password' => ['label'=>'@'],
        'fullname' => ['label'=>'Identit�'],
        'status' => ['label'=>'Etat'],
        'remember_token' => ['label'=>'@'],
        'created_at' => ['label'=>'@'],
        'updated_at' => ['label'=>'@'],
        'deleted_at' => ['label'=>'@'],
    ],

    'accounts' => [
        'list'=>[
            'email'    =>['label'=>'Email de connexion'],
            'fullname' =>['label'=>'Nom Complet'],
            'status'   =>['label'=>'Statut'],
            'roles'    =>['label'=>'Roles'],
        ],
        'fields'=>[
            'email'=>['label'=>'Email de connexion'],
            'fullname'=>['label'=>'Nom Complet'],
            'roles'=>['label'=>'Droits'],
            'change_password'=>['label'=>'Changer le mot de passe'],
            'current_dashboard_id'=>['label'=>'Dashboard actuel de l\'utilisateur'],
            'user_visible'=>['label'=>'Utilisateur Visible ?'],
            'status'=>['label'=>'Etat du Compte'],
        ],

    ],

    'roles_actions'=>[
        'list'=>[
            'name'=>['label'=>'Nom du Groupe']
        ],
        'fields'=>[
            'name'=>['label'=>'Nom du Groupe']
        ]
    ]
];