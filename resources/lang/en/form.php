<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 17/03/2015
 * Time: 19:30
 */
return [
    'users' => [
        'id' => ['label'=>'Id'],
        'email' => ['label'=>'Email'],
        'password' => ['label'=>'@'],
        'fullname' => ['label'=>'Identité'],
        'status' => ['label'=>'Etat'],
        'remember_token' => ['label'=>'@'],
        'created_at' => ['label'=>'@'],
        'updated_at' => ['label'=>'@'],
        'deleted_at' => ['label'=>'@'],
    ]
];