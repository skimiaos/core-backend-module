

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    {{ Assets::favicon('log-favicon.ico') }}

    <title>{{ Config::get('skimia.backend::general.name') }} - Login</title>

    <!-- Bootstrap core CSS -->

    {{ Assets::style('login.min.css') }}
    {{ Assets::style('osml.css', 'skimia.manta') }}
      <style>@spaceless
          input:-webkit-autofill {
              -webkit-box-shadow: 0 0 0px 1000px {{Config::get('skimia.backend::general.login.colors.main')}} inset!important;
          }
          .os-input input{
              border-bottom: 1px solid {{Config::get('skimia.backend::general.login.colors.second')}}!important;
          }
          body{
              background-color: {{Config::get('skimia.backend::general.login.colors.main')}}!important;
          }


          #canvas .layout .layout-row.layout-head{
              background-color: {{Config::get('skimia.backend::general.login.colors.second')}}!important;
          }
      @endspaceless</style>
  </head>

  <body>

      <div id="canvas">

          <div class="layout" >
              <div class="layout-row min-size layout-head">
                  <div class="layout-cell">
                      <h1>{{ Config::get('skimia.backend::general.login.brand') }}</h1>
                      <h2>{{ Config::get('skimia.backend::general.login.title') }}</h2>

                  </div>
              </div>
              @FlashMessages
              <div class="layout-row">
                  <div class="layout-cell">
                      <div class="login-form" ng-cloak>
                          <h2>Accedez au Back-office</h2>


                        {{Skimia\Backend\Data\Forms\Users::render('$api.BackendLogin.POST')}}
                      </div>
                  </div>
              </div>




          </div>

      </div>
      <script>
          var __osml_config = {
              base_path: '../assets/default/default/osml/'
          }
      </script>

      <!-- Bootstrap core JS -->
      {{ Assets::script('jquery.js') }}
      {{ Assets::script('materialize.min.js') }}

  </body>
</html>
