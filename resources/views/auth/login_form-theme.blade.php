


<form ng-submit="{{ AForm::getSubmitAction($angular_submit,'form') }}">
    <div class="row">
        <div class="col s12 m6">{{ a_render_field('email',$fields) }}</div>
        <div class="col s12 m6">{{ a_render_field('password',$fields) }}</div>
        <div class="col s6">{{ a_render_field('remember',$fields) }}</div>
        <div class="col s6">
            <a ng-click="{{ AForm::getSubmitAction($angular_submit,'form') }}" class="btn-flat waves-effect waves-light" type="submit" name="action">{{Config::get('skimia.backend::general.login.button')}}
                <i class="mdi-content-send right"></i>
            </a>
        </div>
    </div>

    <input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/>

</form>
