{{-- Default page layout
  Provides blocks:
    - page.sidenav
    - page.activity
  --}}

<os-container class="layout--container" direction="row">

    @block('page.sidenav')
        @include('skimia.backend::partials.sidenav.default')
    @endshow

    <os-container class="layout--page" direction="column" os-flex="1">
        @block('page.activity')

        @endshow
    </os-container>

</os-container>



