{{-- Configuration page base layout
  Provides blocks:
    - page.content
  --}}

<os-container direction="column" class="layout--page-inner">

        <div class="layout--page-header @{{ activity.bg }}">
            <os-container direction="row" class="layout--page-title row">
                <i class="@{{ icon }} small white-text"></i>
                <h5>@{{ name }}</h5>

                @if($additional_info !== false)
                    <div os-flex="1"></div>
                    <div class="layout--page-title-info message-info">
                        <i class="os-icon-info"></i>
                        <span>{{$additional_info}}</span>
                    </div>

                @endif

            </os-container>
            <os-container direction="row" class="layout--page-actions">
                @block('page.actions')

                @endshow
            </os-container>
        </div>



        @block('page.content')

        @endshow

</os-container>




