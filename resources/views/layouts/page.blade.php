<os-container direction="column" class="layout--page-inner">

    <div class="layout--page-header @{{ activity.bg }}">
        <os-container direction="row" class="layout--page-title row">
            <i class="@{{ icon }} small white-text"></i>
            <h5>@{{ name }}</h5>


        </os-container>
        <os-container direction="row" class="layout--page-actions">
            @block('page.actions')

            @endshow
        </os-container>
    </div>



    @block('page.content')

    @endshow

</os-container>
