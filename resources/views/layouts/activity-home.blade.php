{{-- Activity homepage template
  Provides blocks:
    - page.activity.content
  Implements blocks:
    - page.activity (backend.layouts.default)
  --}}

{{-- Parent layout --}}
@extends('skimia.backend::layouts.default')

{{-- Activity content --}}
@block('page.activity')
  <div class="layout--page-applogo @{{ activity.bg }}">
    <i class="@{{ activity.icon }}"></i>
    <h3>@{{ activity.name }}</h3>
  </div>

  @block('page.activity.content')
    @View
  @endshow
@endblock



