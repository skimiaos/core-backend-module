{{-- Rows page grid
  Provides blocks:
    -
  Implements blocks:
    -  ()
  --}}

<os-container direction="column" class="layout--page-content @block('grid.container.class')@endshow" os-flex="1">
    @foreach($cols as $k => $v)

        <div os-flex="{{$v}}" style="position:relative">
            @block('grid.rows.' . (intval($k) + 1))

            @endshow
        </div>

    @endforeach
</os-container>






