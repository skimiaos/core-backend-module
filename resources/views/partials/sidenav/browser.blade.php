{{-- Sidenav browser-mode partial
  Implements blocks:
    - sidenav.items (backend.partials.sidenav.default)
  --}}

{{-- Parent partial --}}
@extends('skimia.backend::partials.sidenav.default')

{{-- Sidenav items --}}
@block('sidenav.items')
    <os-sidenav-item label="@{{$type}}" icon="@{{typesOptions[$type].icon}}"
                     ng-repeat="($type,def) in typesOptions">
        <os-sidenav-browser ng-model="files[$type].entities" ng-show="files['@{{$type}}']" events="events[$type]"></os-sidenav-browser>
    </os-sidenav-item>
@endoverride


