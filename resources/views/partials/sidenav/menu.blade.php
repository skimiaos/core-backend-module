{{-- Sidenav menu-mode partial
  Implements blocks:
    - sidenav.items (backend.partials.sidenav.default)
  --}}

{{-- Parent partial --}}
@extends('skimia.backend::partials.sidenav.default')

{{-- Sidenav items --}}
@block('sidenav.items')
    <os-sidenav-item label="@{{category.name}}" icon="@{{category.icon}}"
                     ng-repeat="category in categories">
        <os-sidenav-menu ng-hide="Object.keys(category.pages).length">
            <os-sidenav-menu-item name="@{{page.name}}" icon="@{{page.icon}}" active="false"
                                  ng-class="{'active':$state.includes(page.state)}"
                                  ng-click="$state.go(page.state)"
                                  ng-repeat="page in category.pages">@{{page.description}}</os-sidenav-menu-item>
        </os-sidenav-menu>
    </os-sidenav-item>
@endoverride

