{{-- Sidenav default partial
  Provides blocks:
    - sidenav.label
    - sidenav.bg
    - sidenav.items
  --}}

<os-sidenav label="@block('sidenav.label') @{{activity.name}} @endshow"
            color="@block('sidenav.bg') @{{activity.bg}} @endshow">

    @block('sidenav.items')

    @endshow

</os-sidenav>