<os-taskbar>
    <ul class="left">
        <os-taskbar-item icon="mdi-action-dashboard" state="tilescreen" ng-click="$state.go('tilescreen')">Dashboard</os-taskbar-item>
        <os-taskbar-item icon="@{{ activity.icon }}" color="@{{ activity.bg }} " active="activity.active"
                            state="@{{ activity.state }}" ng-click="$state.go(activity.state)"
                            close="activity.close()"
                            ng-repeat="activity in activities|orderObjectBy:'num'">
            @{{ activity.name }}
        </os-taskbar-item>
    </ul>
    <ul class="os-taskbar-actions">
        <div class="os-taskbar-toaster">
            <li>@FlashMessages</li>
        </div>
        <os-taskbar-userpanel username="user.fullname">
            <li class="os-taskbar-userpanel--menu-disconnect">
                <a href="{{url()}}" target="_blank">
                    <i class="os-icon-home-2"></i>
                    Voir le site
                </a>
                <a ng-click="skResponse.get('{{route('backend::logout')}}')">
                    <i class="mdi-action-exit-to-app"></i>
                    Deconnexion
                </a>
            </li>

        </os-taskbar-userpanel>
    </ul>
</os-taskbar>
