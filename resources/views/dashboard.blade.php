{{-- Dashboard template
  Implements blocks:
    - sidenav.label (backend.partials.sidenav)
    - sidenav.bg (backend.partials.sidenav)
  --}}

{{-- Parent layout --}}
@extends('skimia.backend::layouts.default')

{{-- Sidenav configuration --}}
@block('sidenav.label') @{{dashboard.name}} @endoverride
@block('sidenav.bg') grey darken-3 @endoverride

{{-- Page content --}}
@block('page.activity')
    <tilesgrid>
        <grid-column ng-repeat="section in dashboard.sections | orderObjectBy:'position'">
            <h4 class="grid-column-title">@{{section.name}}</h4>
            <tiles-container style="display: none;">
                <tile class="tile-@{{tile.size}} @{{ static_tiles[tile.static_id].bg }}"
                        ng-repeat="tile in dashboard.tiles[section.key] | orderObjectBy:'position'"
                        ng-click="$state.go(static_tiles[tile.static_id].state,static_tiles[tile.static_id].stateParams)">
                    <tile-inner>
                        <i class="@{{static_tiles[tile.static_id].icon}}"
                           ng-class="{'fa-2x':tile.size == 'small',
                                      'fa-3x':tile.size == 'medium',
                                      'fa-5x':tile.size == 'large',
                                      'fa-10x':tile.size == 'huge'}"
                           ng-show="static_tiles[tile.static_id].icon != false"></i>
                        <p ng-hide="tile.size == 'small'">@{{static_tiles[tile.static_id].name}}</p>
                    </tile-inner>
                </tile>
            </tiles-container>
        </grid-column>
    </tilesgrid>
@endoverride
