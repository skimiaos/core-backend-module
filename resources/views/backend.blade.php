<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{{ Config::get('skimia.backend::general.name') }}</title>
        {{ Assets::favicon('os-favicon.ico') }}

        {{-- CSS Includes --}}
        {{ Assets::style('fonticons.css', 'skimia.backend') }}
        {{ Assets::style('ocModal.light.css', 'skimia.angular') }}
        {{ Assets::style('ui-grid.min.css') }}
        {{ Assets::style('backend.min.css') }}
        {{ Assets::style('osml.css', 'skimia.manta') }}
        {{-- End CSS Includes --}}
    </head>

    <body ng-cloak>
        <div class="spashscreen" ng-cloak><p>loading</p></div>

        {{-- UI --}}
        @include('skimia.backend::partials.taskbar')

        <ui-view class="layout--container" name="@{{activity.state}}"
             ng-repeat="activity in activities" ng-show="$state.includes(activity.state)">
        </ui-view>
        <ui-view class="layout--container" name="tilescreen" ng-show="$state.includes('tilescreen')"></ui-view>
        {{-- end UI --}}

        {{-- JS Includes --}}
        {{ Assets::script('jquery.js') }}
        {{ Assets::script('materialize.min.js') }}
        {{-- End JS Includes --}}
    </body>
</html>
