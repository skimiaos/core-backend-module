<?php
$options = [];
$classicOptions = [];

foreach ($field['fields'] as $key=>$sub_field) {
    if(str_contains($key,'-x-')){
        $bread = explode('-x-',$key);
        $sub_field['label'] = end($bread);

        switch(count($bread)){
            case 2:
                if(!isset($options[$bread[0]]))
                    $options[$bread[0]] = [];
                if(!isset($options[$bread[0]]['##']))
                    $options[$bread[0]]['##'] = [];

                $options[$bread[0]]['##'][$key] = $sub_field;
                break;

            //au delas
            default:
                if(!isset($options[$bread[0]]))
                    $options[$bread[0]] = [];

                if(!isset($options[$bread[0]][$bread[1]]))
                    $options[$bread[0]][$bread[1]] = [];

                if(!isset($options[$bread[0]][$bread[1]]['##']))
                    $options[$bread[0]][$bread[1]]['##'] = [];

                $options[$bread[0]][$bread[1]]['##'][$key] = $sub_field;

                break;
        }
    }else
        $classicOptions[$key] = $sub_field;
}
?>

<div class="row">
    <div class="col s12">
        <ul class="tabs">
            <li class="tab"><a href="#general">Actions Générales</a></li>
            @foreach($options as $category=>$sub)
                <li class="tab"><a href="#{{$category}}">{{ucfirst($category)}}</a></li>
            @endforeach
        </ul>
    </div>
    <div id="general" class="col s12 row">

        <h5>Actions Générales</h5>
        @foreach($classicOptions as $key=>$sub_field)
            <div class="col m4 l2">{{AngularFormHelper::render($sub_field['type'], ($name.(Str::endsWith($name,'$index')?'':'\'').'][\''.$key), $sub_field);}}</div>
        @endforeach

    </div>
    @foreach($options as $category=>$sub)
        <div id="{{$category}}" class="col s12">
            <h5>{{ucfirst($category)}}</h5>
            <?php krsort($sub); ?>


            @foreach($sub['##'] as $key=>$sub_field)
                <div class="col m4 l3">{{AngularFormHelper::render($sub_field['type'], ($name.(Str::endsWith($name,'$index')?'':'\'').'][\''.$key), $sub_field);}}</div>
            @endforeach
            <ul class="collapsible col s12" data-collapsible="accordion">
                @foreach($sub as $subkat=>$subsub)

                    @if(isset($subsub['##']))
                    <li>
                        <div class="collapsible-header">{{ucfirst($subkat)}}</div>
                        <div class="collapsible-body">
                            @foreach($subsub['##'] as $key=>$sub_field)
                                <div class="col m4 l3">{{AngularFormHelper::render($sub_field['type'], ($name.(Str::endsWith($name,'$index')?'':'\'').'][\''.$key), $sub_field);}}</div>
                            @endforeach
                        </div>
                    </li>
                    @endif

                @endforeach


            </ul>

        </div>
    @endforeach

</div>

<script>$('ul.tabs').tabs();$('.collapsible').collapsible();</script>



