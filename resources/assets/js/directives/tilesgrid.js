
// <tiles-container>
app.directive('tilesContainer',['$timeout', function($timer){
    return {
        restrict: 'E',
        link: function($scope, element){

            var init = function(){
                var masonry = new Masonry(element[0],{
                    columnWidth: 60,
                    itemSelector: 'tile',
                    gutter: 10
                });
                $(element[0]).fadeIn();
                masonry.layout();
            }

            $timer(init, 0);


            /*var msnry = new Masonry(element[0], {
                columnWidth: 270,
                itemSelector: 'tile',
                gutter: 10
            });

            console.log(msnry);*/
        }
    }
}]);