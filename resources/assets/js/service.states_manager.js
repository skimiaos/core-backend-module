


var activities = angular.module('Activities', ['ct.ui.router.extras','ui.router']);


activities.run(function($rootScope,$deepStateRedirect,$state){
    $rootScope.activities = {};
    var $num = 0;
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){

        if( angular.isDefined(toState.activity) && !angular.isDefined($rootScope.activities[toState.activity.state])&&toState.activity.state != 'tilescreen' ) {
            $num +=1;
            $rootScope.activities[toState.activity.state] = angular.extend({'num':$num,'active':true,
                close:function(){
                    delete $rootScope.activities[toState.activity.state];
                    $deepStateRedirect.reset(toState,toParams);
                    if($state.includes(toState.activity.state))
                        $state.go('tilescreen');
                }
            },toState.activity);

        }

    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){

        angular.forEach($rootScope.activities,function(value,key){
            $rootScope.activities[key]['active'] = false;
        });
        if(angular.isDefined(toState.activity))
            $rootScope.activities[toState.activity.state]['active'] = true;

    });

});

activities.factory('Activity', function($rootScope){

    var Activities = {};


    return Activities;

});