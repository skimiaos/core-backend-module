<?php

$app_os = Angular::application(OS_APPLICATION_NAME)
    ->bind(Config::get('skimia.backend::general.url'),'skimia.backend::backend')
    ->acl('backend',function($context) {
        switch($context){
            case 'html-template':
                echo View::make('skimia.backend::auth.partials.forbidden')->render();
                die();

                break;
            default:
                Redirect::route('angular.application.backend.login')->send();
        }

    });


$app_login = Angular::application(LOGIN_OS_APPLICATION_NAME)
    ->bind(rtrim(Config::get('skimia.backend::general.url'),'/').'/login','skimia.backend::auth.login');
