<?php

/**
 * APPLICATION OS LOGIN
 */
$app_login = Angular::get(LOGIN_OS_APPLICATION_NAME);

$app_login->addDependency('manta', module_assets('skimia/manta','/js/manta.js'));
$app_login->addScript(module_assets('skimia/manta','/js/manta-templates.js'));



/**
 * APPLICATION OS
 */
$app_os = Angular::get(OS_APPLICATION_NAME);

$app_os->addDependency('Activities', module_assets('skimia/backend','/js/service.states_manager.js'));
$app_os->addDependency('ct.ui.router.extras', module_assets('skimia/backend','/js/ct-ui-router-extras.js'));
$app_os->addDependency('wu.masonry', module_assets('skimia/backend','/js/angular-masonry.min.js'));
$app_os->addDependency('ng-context-menu', module_assets('skimia/backend','/js/ng-context-menu.js'));
$app_os->addDependency('manta', module_assets('skimia/manta','/js/manta.js'));
$app_os->addScript(module_assets('skimia/manta','/js/manta-templates.js'));

//UIGRID
$app_os->addDependency('720kb.tooltips', module_assets('skimia/backend','/js/angular-tooltips.min.js'));

$app_os->addDependency('ui.grid', module_assets('skimia/backend','/js/ui-grid.min.js'));
$app_os->addDependency('ui.grid.pagination');
$app_os->addDependency('ui.grid.selection');
$app_os->addDependency('ui.grid.resizeColumns');
$app_os->addDependency('ui.grid.edit');
$app_os->addDependency('ui.grid.rowEdit');
$app_os->addDependency('ui.grid.cellNav');
$app_os->addDependency('ui.grid.moveColumns');
$app_os->addDependency('ui.grid.draggable-rows', module_assets('skimia/backend','/js/draggable-rows.js'));

$app_os->addDependency('ngColorPicker', module_assets('skimia/backend','/js/color-picker.js'));
//voir pour suppression
$app_os->addDependency('ngAria', module_assets('skimia/backend','/js/angular-aria.min.js'));
$app_os->addDependency('ngMaterial', module_assets('skimia/backend','/js/angular-material.min.js'));
$app_os->addDependency('ngTable', module_assets('skimia/backend','/js/ng-table.js'));


$app_os->addScript(module_assets('skimia/backend','/js/TweenMax.min.js'));
// Directives
$app_os->addScriptAfter(module_assets('skimia/backend','/js/directives/tilesgrid.js'));


function os_tooltip($title,$zone = 'top'){
    return ' tooltips title="'.str_replace(['[[',']]'],['{{','}}'],$title).'" tooltip-side="'.$zone.'" tooltip-lazy="true" tooltip-scroll="true" ';
}




//$app_login->addDependency('ngMaterial', module_path('skimia/backend','/js/angular-material.min.js'));
