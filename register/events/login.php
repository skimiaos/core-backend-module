<?php


Event::listen('skimia.backend::login.succeed',function($acl){
    try{
        if($acl->can('backend')) {

            return AResponse::redirect(route('angular.application.backend'))->addMessage(trans('skimia.auth::response.credential.logged-in'))->r();
        }
    }catch(\Exception $e){
        AResponse::addMessage(trans('skimia.backend::response.acl.cant_show_backend'),'danger',12000);
        return AResponse::r();
    }

    AResponse::addMessage(trans('skimia.backend::response.acl.cant_show_backend'),'danger',12000);
},0);