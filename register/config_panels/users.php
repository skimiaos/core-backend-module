<?php


$app_os = Angular::get(OS_APPLICATION_NAME);
//\Skimia\Backend\Data\Forms\UsersCrudForm::register($app_os,'config');
//\Skimia\Backend\Data\Forms\RolesCrudForm::register($app_os,'config');

\Skimia\Backend\Data\Forms\RolesActionsCRUDForm::register($app_os,'config');
\Skimia\Backend\Data\Forms\AccountsCRUDForm::register($app_os,'config');

ConfigPanel::addCategory('users','Utilisateurs','os-icon-users-1');

ConfigPanel::registerState('config.accounts*list', 'users', 'Comptes Utilisateurs', 'Gérer les utilisateurs', 'mdi-social-people','crudform-x-accounts-x-list');
ConfigPanel::registerState('config.roles_actions*list', 'users', 'Groupes Utilisateurs', 'Gérer les autorisations du système', 'os-icon-key-3','crudform-x-roles-actions-x-list');