<?php
Event::listen('skimia.backend::seed.dashboard.default.tiles', function($admin){
    if($admin)
    return ['configuration'=>[
        'zusers'=>[
            'static_id'=> 'config.users',
            'size'=>'medium'
        ]
    ]];
    else
        return [];
});